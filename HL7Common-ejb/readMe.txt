--If you want to use this module, you must import the the Data Below :

INSERT INTO "app_configuration" (id, variable, value) VALUES ('50', 'timeout_for_receiving_messages', '500');
INSERT INTO "app_configuration" (id, variable, value) VALUES ('51', 'xsl_location', 'http://gazelle.ihe.net/xsl/hl7Validation/resultStylesheet.xsl');
INSERT INTO "app_configuration" (id, variable, value) VALUES ('52', 'url_EVCS_ws', 'http://kujira.irisa.fr:8080/Hl7V2Evs-prod-InriaHL7Validator-ejb/InriaHL7CupWS?wsdl');
INSERT INTO "app_configuration" (id, variable, value) VALUES ('53', 'hl7_version', '2.5');
INSERT INTO "app_configuration" (id, variable, value) VALUES ('54', 'application_url', 'http://gazelle.ihe.net/YourSimulator');
INSERT INTO "app_configuration" (id, variable, value) VALUES ('22', 'number_of_segments_to_display', '40');

-- You must put here all the actors you implement
INSERT INTO tf_actor (id, name, keyword, description) VALUES (1, 'Label Broker', 'LB', 'short description');
SELECT pg_catalog.setval('tf_actor_id_seq', 1, true);

-- You must put here all the transactions you implement
INSERT INTO tf_transaction (id, name, keyword, description) VALUES (1, 'LAB-61', 'LAB-61', 'short description');
SELECT pg_catalog.setval('tf_transaction_id_seq', 1, true);

--You must Put here all messages type that your simulator will send and received. The profile_oid can be find at this address : http://gazelle.ihe.net/InriaHL7MessageProfileRepository/home.seam
INSERT INTO "validation_parameters" (id, hl7_version, message_type,profile_oid,actor_id,transaction_id) VALUES ('1','2.5', 'OML^O33^OML_O33','1.3.6.1.4.12559.11.1.1.136',2,1);
--It is also necessary for the message with the code ACK
INSERT INTO "validation_parameters" (id, message_type,profile_oid,actor_id,transaction_id) VALUES ('5', 'ACK^ALL^ACK','1.3.6.1.4.12559.11.1.1.22',2,1);

SELECT pg_catalog.setval('ort_validation_parameters_id_seq', 1, true);

INSERT INTO affinity_domain (id, keyword, label_to_display, description) VALUES (1, 'IHE', 'IHE', 'IHE');
SELECT pg_catalog.setval('affinity_domain_id_seq', 1, true);

INSERT INTO hl7_validation_parameters_affinity_domain (affinity_domain_id, validation_parameters_id) VALUES (1,1); 

-- set there the charset used by your simulator
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (1,  'ASCII','ASCII', 'US-ASCII' );                                
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (2,  'ISO8859-1',	'8859/1'	, 'ISO8859_1');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (3,  'ISO8859-2', 	'8859/2'	, 'ISO8859_2');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (4,  'ISO8859-3', 	'8859/3'	, 'ISO8859_3');    
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (5,  'ISO8859-4', 	'8859/4'	, 'ISO8859_4');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (6,  'ISO8859-5', 	'8859/5'	, 'ISO8859_5');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (7,  'ISO8859-6', 	'8859/6'	, 'ISO8859_6');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (8,  'ISO8859-7', 	'8859/7'	, 'ISO8859_7');   
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (9,  'ISO8859-8', 	'8859/8'	, 'ISO8859_8');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (10, 'ISO8859-9', 	'8859/9'	, 'ISO8859_9');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (11, 'Japanese (JIS X 0201)', 	'ISO IR14', 'JIS0201' );
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (12, 'Japanese (JIS X 0208)', 	'ISO IR87', 'JIS0208' );            
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (13, 'Japanese (JIS X 0212)', 	'ISO IR159',  'JIS0212');       
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (14, 'Chinese GB 18030-2000', 	'GB 18030-2000'	, 'EUC_CN');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (15, 'Korean KS X 1001', 	'KS X 1001'	, 'EUC_KR');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (16, 'Taiwanese CNS 11643-1992', 	'CNS 11643-1992', 'EUC_TW');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (17, 'Taiwanese (BIG 5)', 'BIG-5', 'Big5');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (18, 'UTF-8', 		'UNICODE UTF-8' , 'UTF8'    ); 
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (19, 'UTF-16', 		'UNICODE UTF-16', 'UTF-16');
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (20, 'UTF-32', 		'UNICODE UTF-32', 'UTF-32');   
INSERT INTO hl7_charset (id, display_name, hl7_code, java_code) VALUES (21, 'ISO8859-15', '8859/15', 'ISO8859_15');
SELECT pg_catalog.setval('hl7_charset_id_seq', 21, true);

-- set there the configuration of the simulated actors acting as responders
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, affinity_domain_id, charset_id, accepted_message_type, accepted_trigger_event) VALUES (1, 'LB in UTF-8', 10010, 1, 1, 1, 1, '*', '*');
INSERT INTO hl7_simulator_responder_configuration (id, name, listening_port, simulated_actor_id, transaction_id, affinity_domain_id, charset_id, accepted_message_type, accepted_trigger_event) VALUES (2, 'LB in ASCII', 10011, 1, 1, 1, 2, '*', '*');
SELECT pg_catalog.setval('hl7_simulator_responder_configuration_id_seq', 2, true);

-- If you use your simulator has a responder and have a hibernate.cfg.xml file, add the following lines:
<mapping class="net.ihe.gazelle.HL7Common.model.HL7Message" />
<mapping class="net.ihe.gazelle.HL7Common.model.AffinityDomain" />
<mapping class="net.ihe.gazelle.simulator.common.tf.model.Actor" />
<mapping class="net.ihe.gazelle.simulator.common.tf.model.Transaction" />
<mapping class="net.ihe.gazelle.HL7Common.model.SystemConfiguration" />
<mapping class="net.ihe.gazelle.HL7Common.model.EVSCValidationResults" />
<mapping class="net.ihe.gazelle.simulator.common.model.ApplicationConfiguration" />
<mapping class="net.ihe.gazelle.HL7Common.model.Charset" />
<mapping class="net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration" />


INSERT INTO "app_configuration" (id, variable, value) VALUES ('22', 'number_of_segments_to_display', '40');

-- How to fill the package_name_for_profile_oid, example :
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (2, '1.3.6.1.4.12559.11.1.1.135', 'net.ihe.gazelle.laboratory.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (3, '1.3.6.1.4.12559.11.1.1.137', 'net.ihe.gazelle.laboratory.hl7v2.model.v25.message');
INSERT INTO "package_name_for_profile_oid" (id, profile_oid, package_name) VALUES (4, '1.3.6.1.4.12559.11.1.1.134', 'net.ihe.gazelle.laboratory.hl7v2.model.v25.message');

