package net.ihe.gazelle.HL7Common.initiator;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.hoh.api.DecodeException;
import ca.uhn.hl7v2.hoh.api.EncodeException;
import ca.uhn.hl7v2.hoh.api.IReceivable;
import ca.uhn.hl7v2.hoh.api.ISendable;
import ca.uhn.hl7v2.hoh.hapi.api.MessageSendable;
import ca.uhn.hl7v2.hoh.hapi.client.HohClientSimple;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.utils.GazelleHapiContext;
import net.ihe.gazelle.HL7Common.utils.HL7MessageUtils;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * <p>GazelleHL7v2Client class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class GazelleHL7v2Client {

    private static final int DEFAULT_TIMEOUT = 30000;

    private static Logger log = LoggerFactory.getLogger(GazelleHL7v2Client.class);

    /**
     * <p>sendMessageOverMllp.</p>
     *
     * @param messageToSend a {@link ca.uhn.hl7v2.model.Message} object.
     * @param ipAddress a {@link java.lang.String} object.
     * @param port a {@link java.lang.Integer} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a {@link ca.uhn.hl7v2.model.Message} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public static Message sendMessageOverMllp(Message messageToSend, String ipAddress, Integer port, EntityManager entityManager) throws HL7Exception {
        Message response = null;
        Connection connection = null;
        GazelleHapiContext context = GazelleHapiContext.instance();
        Exception exception = null;
        try {
            connection = context.newClient(ipAddress, port, false);
            Initiator initiator = connection.getInitiator();
            initiator.setTimeout(getTimeout(entityManager), TimeUnit.MILLISECONDS);
            response = initiator.sendAndReceive(messageToSend);
        } catch (Exception e) {
           exception = e;
        } finally {
            if (connection != null) {
                connection.close();
            }
            try{
                context.close();
            }catch (IOException e){
                log.warn("GazelleHapiContext cannot be closed");
            }
        }
        if (exception != null){
            throw new HL7Exception("Error occurred during the transaction with " + ipAddress
                    + ": " + exception.getClass().getName() + ": " + exception.getMessage());
        } else {
            return response;
        }
    }

    /**
     * <p>sendMessageOverMllp.</p>
     *
     * @param messageToSend a {@link ca.uhn.hl7v2.model.Message} object.
     * @param sutConfiguration a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a {@link ca.uhn.hl7v2.model.Message} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    @SuppressWarnings("resource")
    public static Message sendMessageOverMllp(Message messageToSend, HL7V2ResponderSUTConfiguration sutConfiguration, EntityManager entityManager)
            throws HL7Exception {
        return sendMessageOverMllp(messageToSend, sutConfiguration.getIpAddress(), sutConfiguration.getPort(), entityManager);
    }

    /**
     * <p>sendMessageOverHttp.</p>
     *
     * @param messageToSend a {@link ca.uhn.hl7v2.model.Message} object.
     * @param sutConfiguration a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a {@link ca.uhn.hl7v2.model.Message} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public static Message sendMessageOverHttp(Message messageToSend, HL7V2ResponderSUTConfiguration sutConfiguration, EntityManager entityManager)
            throws HL7Exception {
        Parser parser = HL7MessageUtils.getParserForMessageEncoding(sutConfiguration.getMessageEncoding());
        Message response;
        HohClientSimple client = null;
        try {
            client = new HohClientSimple(new URL(sutConfiguration.getUrl()), parser);
            client.setResponseTimeout(getTimeout(entityManager));
            ISendable<Message> sendable = new MessageSendable(messageToSend);
            // sendAndReceive actually sends the message
            IReceivable<Message> receivable = client.sendAndReceiveMessage(sendable);
            response = receivable.getMessage();
        } catch (IOException|EncodeException|DecodeException e) {
            log.error(e.getMessage(), e);
            throw new HL7Exception("Error occurred during the transaction with " + sutConfiguration.getIpAddress()
                    + ": " + e.getClass().getName() + ": " + e.getMessage());
        } finally {
            if (client != null) {
                client.close();
            }
        }
        return response;
    }

    /**
     * <p>sendMessageOverMllpAndGetResponseAsString.</p>
     *
     * @param messageToSend a {@link ca.uhn.hl7v2.model.Message} object.
     * @param sutConfiguration a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     * @return a {@link java.lang.String} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public static String sendMessageOverMllpAndGetResponseAsString(Message messageToSend,
                                                                   HL7V2ResponderSUTConfiguration sutConfiguration) throws HL7Exception {
        Message response = sendMessageOverMllp(messageToSend, sutConfiguration, null);
        if (response != null) {
            return response.encode();
        } else {
            return null;
        }
    }

    /**
     * Override the DEFAULT_TIMEOUT value using the timeout_for_receiving_messages preferences from the database
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     * @return a int.
     */
    public static int getTimeout(EntityManager entityManager) {
        String timeoutAsString = ApplicationConfiguration.getValueOfVariable("timeout_for_receiving_messages", entityManager);
        if (timeoutAsString == null) {
            log.error("The variable timeout_for_receiving_messages on the app_configuration Table is empty or missing ! The default value is 30000 ms.");
            return DEFAULT_TIMEOUT;
        } else {
            try {
                return Integer.parseInt(timeoutAsString);
            } catch (NumberFormatException e) {
                log.error("The variable timeout_for_receiving_messages is not a well-formed integer");
                return DEFAULT_TIMEOUT;
            }

        }
    }

}
