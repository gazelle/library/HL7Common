package net.ihe.gazelle.HL7Common.ws;

import net.ihe.gazelle.simulator.common.model.ApplicationConfigurationQuery;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstanceQuery;
import net.ihe.gazelle.simulator.report.TestReportData;
import net.ihe.gazelle.simulator.report.TestReportDataMessage;
import net.ihe.gazelle.simulator.report.TestReportTool;
import net.ihe.gazelle.simulator.ws.TestReport;
import net.ihe.gazelle.simulator.ws.TestReportLocal;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import javax.ejb.Stateless;
import java.util.Arrays;

@Stateless
/**
 * <p>HL7v2TestReport class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class HL7v2TestReport extends TestReport implements TestReportLocal {

    private static Log log = Logging.getLog(HL7v2TestReport.class);

    /** {@inheritDoc} */
    @Override
    protected TestReportData newInstance(String testId, String test) {
        TestReportData report = new TestReportData(testId, test);
        setTestResults(report);
        ApplicationConfigurationQuery appConfQuery = new ApplicationConfigurationQuery();
        appConfQuery.variable().eq("application_name");
        String applicationName = null;
        try {
            applicationName = appConfQuery.getUniqueResult().getValue();
        } catch (NullPointerException e) {
            log.error("No value set for application_name in app_configuration table");
        }
        appConfQuery = new ApplicationConfigurationQuery();
        appConfQuery.variable().eq("application_url");
        String applicationUrl = null;
        try {
            applicationUrl = appConfQuery.getUniqueResult().getValue();
        } catch (NullPointerException e) {
            log.error("No value set for application_url in app_configuration table");
        }
        TestReportTool tool = new TestReportTool(applicationName, applicationUrl);
        report.setTool(tool);
        return report;
    }

    /**
     * <p>setTestResults.</p>
     *
     * @param report a {@link net.ihe.gazelle.simulator.report.TestReportData} object.
     */
    protected void setTestResults(TestReportData report) {
        if (report.getTestId() == null || report.getTestId().isEmpty()) {
            log.error("received testId is null or empty !");
            return;
        }
        TransactionInstanceQuery query = new TransactionInstanceQuery();
        query.id().eq(Integer.decode(report.getTestId()));
        TransactionInstance hl7Message = query.getUniqueResult();
        if (hl7Message == null) {
            log.error("no message found with id " + report.getTestId());
            return;
        } else {
            report.setTest(null);
            report.setSource(null);
            report.setTarget(null);
            if (hl7Message.getTransaction() != null) {
                report.setTest(hl7Message.getTransaction().getKeyword());
            }
            report.setSource(hl7Message.getRequest().getIssuer());
            report.setTarget(hl7Message.getResponse().getIssuer());
            TestReportDataMessage request = new TestReportDataMessage("request", hl7Message.getRequest().getType(),
                    hl7Message.getRequest().getValidationStatus(), hl7Message.getRequest().getIssuingActor().getKeyword(), null);
            TestReportDataMessage response = new TestReportDataMessage("response", hl7Message.getResponse().getType(),
                    hl7Message.getResponse().getValidationStatus(), hl7Message.getResponse().getIssuingActor().getKeyword(), null);
            report.setMessages(Arrays.asList(request, response));
            report.setAcknowledgmentCode(null);
            if (hl7Message.getResponse() != null && !hl7Message.getResponse().getContentAsString().isEmpty()) {
                String acknowledgmentCode = hl7Message.getResponse().getContentAsString();
                int a = acknowledgmentCode.indexOf("MSA");
                if (a != -1) {
                    acknowledgmentCode = acknowledgmentCode.substring(a);
                    if (acknowledgmentCode.length() > 5) {
                        acknowledgmentCode = acknowledgmentCode.substring(4, 6);
                    } else {
                        acknowledgmentCode = "";
                    }
                } else {
                    acknowledgmentCode = "";
                }
                report.setAcknowledgmentCode(acknowledgmentCode);
            }
            report.setDate(hl7Message.getTimestamp());
            ApplicationConfigurationQuery appConfQuery = new ApplicationConfigurationQuery();
            appConfQuery.variable().eq("application_url");
            try {
                report.setLogUrl(appConfQuery.getUniqueResult().getValue().concat("/message.seam?id=").concat(report.getTestId()));
            } catch (NullPointerException e) {
                log.error("No value set for application_name in app_configuration table");
            }
        }
    }
}
