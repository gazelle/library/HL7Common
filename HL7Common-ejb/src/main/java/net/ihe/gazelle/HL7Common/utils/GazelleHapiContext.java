package net.ihe.gazelle.HL7Common.utils;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.llp.MinLowerLayerProtocol;
import ca.uhn.hl7v2.validation.impl.NoValidation;

import java.io.IOException;

/**
 * <b>Class Description : </b>GazelleHapiContext<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 10/11/15
 */
public class GazelleHapiContext extends DefaultHapiContext {

    /**
     * <p>Constructor for GazelleHapiContext.</p>
     */
    public GazelleHapiContext(){
        super(new NoValidation());
        setLowerLayerProtocol(new MinLowerLayerProtocol(true));
    }

    /**
     * <p>instance.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.utils.GazelleHapiContext} object.
     */
    public static GazelleHapiContext instance(){
        return new GazelleHapiContext();
    }

    /** {@inheritDoc} */
    @Override
    public void close() throws IOException {
        this.getConnectionHub().discardAll();
    }
}
