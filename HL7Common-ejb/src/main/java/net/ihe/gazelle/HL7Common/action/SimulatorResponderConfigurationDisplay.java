package net.ihe.gazelle.HL7Common.action;

import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfigurationQuery;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;

import java.io.Serializable;
import java.util.List;

/**
 * <p>Abstract SimulatorResponderConfigurationDisplay class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public abstract class SimulatorResponderConfigurationDisplay implements Serializable {


    /**
     *
     */
    private static final long serialVersionUID = 45456468478L;

    protected List<SimulatorResponderConfiguration> configurations;

    /**
     * <p>Getter for the field <code>configurations</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SimulatorResponderConfiguration> getConfigurations() {
        return configurations;
    }

    /**
     * returns the URL to the list of messages exchanged with this part of the tool
     *
     * @param conf a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     * @return a {@link java.lang.String} object.
     */
    public abstract String getUrlForHL7Messages(SimulatorResponderConfiguration conf);

    /**
     * <p>getSimulatorResponderConfiguration.</p>
     */
    public abstract void getSimulatorResponderConfiguration();

    /**
     * <p>getUrlForConsole.</p>
     *
     * @param conf a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     * @return a {@link java.lang.String} object.
     */
    public String getUrlForConsole(SimulatorResponderConfiguration conf){
        StringBuilder url = new StringBuilder("/console/receiverConsole.seam?simulatedActor=");
        url.append(conf.getSimulatedActor().getId());
        url.append("&domain=");
        url.append(conf.getDomain().getId());
        if (conf.getTransaction() != null){
            url.append("&transaction=");
            url.append(conf.getTransaction().getId());
        }
        return url.toString();
    }

    /**
     * <p>getSimulatorResponderConfigurations.</p>
     *
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public void getSimulatorResponderConfigurations(Actor simulatedActor, Transaction transaction, Domain domain) {
        SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery();
        query.name().order(true);
        if (simulatedActor != null) {
            query.simulatedActor().eq(simulatedActor);
        }
        if (transaction != null) {
            query.transaction().eq(transaction);
        }
        if (domain != null) {
            query.domain().eq(domain);
        }
        configurations = query.getList();
    }
}
