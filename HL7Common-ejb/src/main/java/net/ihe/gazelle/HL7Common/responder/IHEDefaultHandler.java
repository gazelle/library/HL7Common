package net.ihe.gazelle.HL7Common.responder;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.protocol.MetadataKeys;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;
import net.ihe.gazelle.simulator.common.model.ReceiverConsole;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.Map;

/**
 * Class description : <b>IHEDefaultHandler</b>
 * This class is aimed to be extends by handlers implemented for the different responders
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes Bretagne Atlantique
 * @version 1.0 - 2011, July 18th
 */
public abstract class IHEDefaultHandler implements ReceivingApplication {

    private static Logger log = LoggerFactory.getLogger(IHEDefaultHandler.class);

    protected EntityManager entityManager;
    protected String sendingApplication;
    protected String sendingFacility;
    protected String receivingApplication;
    protected String receivingFacility;
    protected String hl7Charset;
    protected Terser terser;
    protected String messageControlId;
    protected String messageType;
    protected String hl7Version;
    private TransactionInstance transactionInstance;
    private String triggerEvent;
    private String responseMessageType;
    private Actor sutActor;

    /**
     * The expected receivingApplication value and the sendingApplication value for reply
     */
    protected String serverApplication;
    /**
     * The expected receivingFacility value and the sendingFacility value for reply
     */
    protected String serverFacility;
    /**
     * The actor simulated by the handler
     */
    protected Actor simulatedActor;
    /**
     * The transaction simulated by the handler
     */
    protected Transaction simulatedTransaction;
    /**
     * The affinity domain simulated by the handler
     */
    protected Domain domain;

    protected Map<String, Object> metadata;

    /**
     * <p>Constructor for IHEDefaultHandler.</p>
     */
    public IHEDefaultHandler() {
        super();
    }

    /**
     * Create a new instance of IHEDefaultHandler from the current one
     *
     * @return a {@link net.ihe.gazelle.HL7Common.responder.IHEDefaultHandler} object.
     */
    public abstract IHEDefaultHandler newInstance();

    /**
     * <p>Getter for the field <code>serverApplication</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getServerApplication() {
        return serverApplication;
    }

    /**
     * <p>Setter for the field <code>serverApplication</code>.</p>
     *
     * @param serverApplication a {@link java.lang.String} object.
     */
    public void setServerApplication(String serverApplication) {
        this.serverApplication = serverApplication;
    }

    /**
     * <p>Getter for the field <code>serverFacility</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getServerFacility() {
        return serverFacility;
    }

    /**
     * <p>Setter for the field <code>serverFacility</code>.</p>
     *
     * @param serverFacility a {@link java.lang.String} object.
     */
    public void setServerFacility(String serverFacility) {
        this.serverFacility = serverFacility;
    }

    /**
     * <p>Getter for the field <code>simulatedActor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getSimulatedActor() {
        return simulatedActor;
    }

    /**
     * <p>Setter for the field <code>simulatedActor</code>.</p>
     *
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setSimulatedActor(Actor simulatedActor) {
        this.simulatedActor = simulatedActor;
    }

    /**
     * <p>Getter for the field <code>simulatedTransaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getSimulatedTransaction() {
        return simulatedTransaction;
    }

    /**
     * <p>Setter for the field <code>simulatedTransaction</code>.</p>
     *
     * @param simulatedTransaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public void setSimulatedTransaction(Transaction simulatedTransaction) {
        this.simulatedTransaction = simulatedTransaction;
    }

    /**
     * <p>Getter for the field <code>domain</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public Domain getDomain() {
        return domain;
    }

    /**
     * <p>Setter for the field <code>domain</code>.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    /**
     * {@inheritDoc}
     * Returns true if this Application wishes to accept the message. By
     * returning true, this Application declares itself the recipient of the
     * message, accepts responsibility for it, and must be able to respond
     * appropriately to the sending system.
     */
    @Override
    public boolean canProcess(Message incomingMessage) {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public Message processMessage(final Message incomingMessage, Map<String, Object> inMetadata)
            throws ReceivingApplicationException, HL7Exception {
        this.metadata = inMetadata;
        try {
            final Object response = HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                @Override
                public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                    setEntityManager(entityManager);
                    // copy the incomingMessage to make sure it will not be altered before being stored
                    PipeParser parser = PipeParser.getInstanceWithNoValidation();
                    String originalRequestMessage;
                    try {
                         originalRequestMessage = parser.encode(incomingMessage);
                    }catch (HL7Exception e){
                        originalRequestMessage = null;
                    }
                    if (incomingMessage == null) {
                        return null;
                    } else {
                        extractInformations(incomingMessage);
                        Object responseObject = null;
                        try {
                            responseObject = processMessage(incomingMessage);
                            if (responseObject == null){
                                responseObject = incomingMessage.generateACK(AcknowledgmentCode.AE, new HL7Exception("Cannot process message"));
                            }
                        } catch (Exception e) {
                            responseObject = incomingMessage.generateACK(AcknowledgmentCode.AE, new HL7Exception(e));
                            logIfError((Message) responseObject, entityManager);
                        } finally {
                            Message responseMessage = (Message) responseObject;
                            saveTransactionInstance(originalRequestMessage, responseMessage);

                        }
                        return responseObject;
                    }
                }
            });

            if (response instanceof Message) {
                return (Message) response;
            }
        } catch (HibernateFailure e) {
            // already logged
        }
        return null;
    }

    /**
     * <p>processMessage.</p>
     *
     * @param incomingMessage a {@link ca.uhn.hl7v2.model.Message} object.
     *
     * @return a {@link ca.uhn.hl7v2.model.Message} object.
     *
     * @throws ca.uhn.hl7v2.protocol.ReceivingApplicationException if any.
     * @throws ca.uhn.hl7v2.HL7Exception                           if any.
     */
    @Transactional
    public abstract Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception;

    /**
     * <p>getSenderIpAddress.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    private String getSenderIpAddress() {
        if (this.metadata != null) {
            return (String) metadata.get(MetadataKeys.IN_SENDING_IP);
        } else {
            return null;
        }
    }

    /**
     * <p>getMessageControlID.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageControlID() {
        if (this.metadata != null) {
            return (String) metadata.get(MetadataKeys.IN_MESSAGE_CONTROL_ID);
        } else {
            return null;
        }
    }




    protected void saveTransactionInstance(String incomingMessage, Message responseMessage) {
        PipeParser parser = PipeParser.getInstanceWithNoValidation();
        String responseString = null;
        String requestString = incomingMessage;
        if (responseMessage != null){
            try {
                responseString = parser.encode(responseMessage);
            }catch (HL7Exception e){
                responseString = null;
            }
        }
        String sutIpAddress = getSenderIpAddress();
        transactionInstance = TransactionInstanceUtil.newInstance(sendingApplication, sendingFacility, receivingApplication,
                receivingFacility, requestString, responseString, new Date(), simulatedTransaction, messageType,
                responseMessageType, simulatedActor, sutActor, simulatedActor, domain, sutIpAddress);
        // just in case the transaction was rollback
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
        }
        transactionInstance = transactionInstance.save(entityManager);
        if (responseMessage != null) {
            TransactionInstanceUtil.extractMessageControlId(responseMessage, transactionInstance.getResponse());
        }
        TransactionInstanceUtil.setMessageControlId(messageControlId, transactionInstance.getRequest());
    }

    /**
     * <p>logIfError.</p>
     *
     * @param response      a {@link ca.uhn.hl7v2.model.Message} object.
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     */
    protected void logIfError(Message response, EntityManager entityManager) {
        Terser terser = new Terser(response);
        try {
            Segment err = terser.getSegment("/.ERR");
            Segment msa = terser.getSegment("/.MSA");
            if (err.isEmpty()) {
                return;
            } else {
                String code = msa.getField(1, 0).encode();
                String comment = err.getField(8, 0).encode();
                String sut = sendingApplication + "_" + sendingFacility;
                ReceiverConsole.newLog(simulatedActor, simulatedTransaction, domain, triggerEvent, messageControlId, sut, code, comment,
                        entityManager);
            }
        } catch (HL7Exception e) {
            log.debug("Cannot access segment in message: " + e.getMessage());
            return;
        }
    }

    /**
     * <p>Setter for the field <code>entityManager</code>.</p>
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * <p>extractInformations.</p>
     *
     * @param incomingMessage a {@link ca.uhn.hl7v2.model.Message} object.
     *
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public void extractInformations(Message incomingMessage) throws HL7Exception {
        terser = new Terser(incomingMessage);
        sendingApplication = terser.get("/.MSH-3-1");
        sendingFacility = terser.get("/.MSH-4-1");
        receivingApplication = terser.get("/.MSH-5-1");
        receivingFacility = terser.get("/.MSH-6-1");
        hl7Charset = terser.get("/.MSH-18");
        messageControlId = getMessageControlID();
        messageType = HL7MessageDecoder.getMessageType(incomingMessage);
        hl7Version = terser.get("/.MSH-12-1");
        triggerEvent = terser.get("/MSH-9-2");
    }


    protected void setResponseMessageType(String responseMessageType) {
        this.responseMessageType = responseMessageType;
    }

    protected void setSutActor(Actor sutActor) {
        this.sutActor = sutActor;
    }

    protected void setIncomingMessageType(String incomingMessageType) {
        this.messageType = incomingMessageType;
    }

    protected String getIncomingTriggerEvent(){
        return this.triggerEvent;
    }
}