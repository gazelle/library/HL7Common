package net.ihe.gazelle.HL7Common.action;

import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.HL7Common.responder.ServerLocal;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PreDestroy;
import javax.ejb.Remove;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.List;

/**
 * <b>Class Description : </b>SimulatorResponderStartStop<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 14/09/15
 */
@Name("simulatorResponderStartStop")
@Scope(ScopeType.APPLICATION)
public class SimulatorResponderStartStop implements Serializable {

    private static Logger log = LoggerFactory.getLogger(SimulatorResponderStartStop.class);


    /**
     * <p>stopAllServers.</p>
     */
    public void stopAllServers() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        List<SimulatorResponderConfiguration> configurations = SimulatorResponderConfiguration
                .getConfigurationsFiltered(null, null, null, null, entityManager, true, HL7Protocol.MLLP);
        if (configurations != null) {
            for (SimulatorResponderConfiguration config : configurations) {
                ServerLocal server = (ServerLocal) Component.getInstance(config.getServerBeanName());
                if (server != null) {
                    config.setRunning(!server.stopServer(config.getName()));
                    try {
                        config.save(entityManager);
                    } catch (Exception e) {
                        FacesMessages.instance().add(
                                "Cannot save the new state of the configuration " + config.getName());
                    }
                } else {
                    log.error("No bean found with name " + config.getName());
                    FacesMessages.instance().add(config.getName() + " has not been found");
                }
            }

        } else {
            FacesMessages.instance().add(StatusMessage.Severity.INFO,"No server is running");
        }
    }

    /**
     * <p>startAllServers.</p>
     */
    public void startAllServers() {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        List<SimulatorResponderConfiguration> configurations = SimulatorResponderConfiguration
                .getConfigurationsFiltered(null, null, null, null, entityManager, false, HL7Protocol.MLLP);
        if (configurations != null) {
            for (SimulatorResponderConfiguration config : configurations) {
                ServerLocal server = (ServerLocal) Component.getInstance(config.getServerBeanName());
                if (server != null) {
                    config.setRunning(server.startServer(config.getName()));
                    try {
                        config.save(entityManager);
                    } catch (Exception e) {
                        FacesMessages.instance().add(
                                "Cannot save the new state of the configuration " + config.getName());
                    }
                } else {
                    log.error("No bean found with name " + config.getServerBeanName());
                    FacesMessages.instance().add(config.getServerBeanName() + " has not been found");
                }
            }

        } else {
            FacesMessages.instance().add(StatusMessage.Severity.INFO,"No stopped server found");
        }
    }

}
