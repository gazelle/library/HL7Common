package net.ihe.gazelle.HL7Common.ws;


import net.ihe.gazelle.HL7Common.messages.AsynchHL7ValidationExecutor;
import net.ihe.gazelle.HL7Common.messages.IAsynchHL7ValidationExecutor;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.MessageInstanceDAO;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstanceDAO;
import org.jboss.seam.annotations.In;
import org.jboss.seam.async.QuartzDispatcher;
import org.jboss.seam.contexts.Lifecycle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


@Stateless
public class ValidationAPIImpl implements ValidationAPI {

    private static final String NO_MATCH = "Warning: 199 PatientManager \"NAV: No message found with the given parameters \"";
    private static final String WRONG_REQUEST = "Warning: 199 PatientManager \"NAV: both the message ID and the issuer are required \"";
    private static final String WARNING = "Warning";
    private static final Logger LOG = LoggerFactory.getLogger(ValidationAPIImpl.class);

    @Override
    public Response launchValidation(String messageId, String issuer) {
        LOG.info("request received");
        if (messageId == null || issuer == null){
            return Response.status(400).header(WARNING, WRONG_REQUEST).build();
        }
        Lifecycle.beginCall();
        TransactionInstance transactionInstance = TransactionInstanceDAO.getTransactionInstanceByMessageIdAndIssuer(messageId, issuer);
        if (transactionInstance == null){
            Lifecycle.endCall();
            return Response.status(404).header(WARNING, NO_MATCH).build();
        }
        QuartzDispatcher.instance().scheduleAsynchronousEvent(AsynchHL7ValidationExecutor.getObserverName(), transactionInstance);
        Lifecycle.endCall();
        LOG.info("ready to send back response");
        return Response.ok(transactionInstance.getId()).build();
    }

    @Override
    public Response validationStatus(String messageId, String issuer) {
        if (messageId == null || issuer == null){
            return Response.status(400).header(WARNING, WRONG_REQUEST).build();
        }
        Lifecycle.beginCall();
        MessageInstance messageInstance = MessageInstanceDAO.getMessageInstanceByMessageIdAndIssuer(messageId, issuer);
        Lifecycle.endCall();
        if (messageInstance != null){
            return Response.ok(messageInstance.getValidationStatus()).build();
        } else {
            return Response.status(404).header(WARNING, NO_MATCH).build();
        }
    }

    @Override
    public Response validationReport(String messageId, String issuer) {
        if (messageId == null || issuer == null){
            return Response.status(400).header(WARNING, WRONG_REQUEST).build();
        }
        Lifecycle.beginCall();
        MessageInstance messageInstance = MessageInstanceDAO.getMessageInstanceByMessageIdAndIssuer(messageId, issuer);
        Lifecycle.endCall();
        if (messageInstance == null){
            return Response.status(404).header(WARNING, NO_MATCH).build();
        } else if (messageInstance.getValidationDetailedResult() == null) {
            return Response.status(404).header(WARNING, "Warning: 199 PatientManager \"NAV: Validation report not available \"").build();
        } else {
            String validationReport = new String(messageInstance.getValidationDetailedResult(), StandardCharsets.UTF_8);
            return Response.ok(validationReport).build();
        }
    }


}