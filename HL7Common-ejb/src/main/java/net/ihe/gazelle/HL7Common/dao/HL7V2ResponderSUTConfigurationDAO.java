package net.ihe.gazelle.HL7Common.dao;

import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfigurationQuery;

/**
 * <p>HL7V2ResponderSUTConfigurationDAO class.</p>
 *
 * @author aberge
 * @version 1.0: 04/10/17
 */

public class HL7V2ResponderSUTConfigurationDAO {

    public static HL7V2ResponderSUTConfiguration getSUTByName(String sutsName) {
        HL7V2ResponderSUTConfigurationQuery query = new HL7V2ResponderSUTConfigurationQuery();
        query.name().eq(sutsName);
        return query.getUniqueResult();
    }
}
