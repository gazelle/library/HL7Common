package net.ihe.gazelle.HL7Common.action;

import net.ihe.gazelle.HL7Common.model.HL7Protocol;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration;
import net.ihe.gazelle.HL7Common.model.SimulatorResponderConfigurationQuery;
import net.ihe.gazelle.HL7Common.responder.ServerLocal;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import org.hibernate.exception.ConstraintViolationException;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.model.SelectItem;
import java.io.Serializable;
import java.sql.BatchUpdateException;
import java.util.List;

/**
 * <p>SimulatorResponderConfigurationManager class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("simulatorResponderConfigurationManager")
@Scope(ScopeType.PAGE)
public class SimulatorResponderConfigurationManager implements Serializable {

    private static Logger log = LoggerFactory.getLogger(SimulatorResponderConfigurationManager.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private SimulatorResponderConfiguration selectedConfiguration = null;
    private boolean editConfig = false;
    private boolean displayConfig = false;
    private boolean listConfig = true;
    private String selectedBeanName = null;
    private Filter<SimulatorResponderConfiguration> filter;

    private Filter<SimulatorResponderConfiguration> getFilter(){
        if (filter == null){
            SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery();
            filter = new Filter<SimulatorResponderConfiguration>(query.getHQLCriterionsForFilter());
        }
        return filter;
    }


    /**
     * <p>Getter for the field <code>selectedConfiguration</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public SimulatorResponderConfiguration getSelectedConfiguration() {
        return selectedConfiguration;
    }

    /**
     * <p>Setter for the field <code>selectedBeanName</code>.</p>
     *
     * @param selectedBeanName a {@link java.lang.String} object.
     */
    public void setSelectedBeanName(String selectedBeanName) {
        this.selectedBeanName = selectedBeanName;
    }

    /**
     * <p>Getter for the field <code>selectedBeanName</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectedBeanName() {
        return selectedBeanName;
    }

    /**
     * <p>Setter for the field <code>selectedConfiguration</code>.</p>
     *
     * @param selectedConfiguration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public void setSelectedConfiguration(SimulatorResponderConfiguration selectedConfiguration) {
        this.selectedConfiguration = selectedConfiguration;
    }

    /**
     * <p>isEditConfig.</p>
     *
     * @return a boolean.
     */
    public boolean isEditConfig() {
        return editConfig;
    }

    /**
     * <p>Setter for the field <code>editConfig</code>.</p>
     *
     * @param editConfig a boolean.
     */
    public void setEditConfig(boolean editConfig) {
        this.editConfig = editConfig;
    }

    /**
     * <p>isDisplayConfig.</p>
     *
     * @return a boolean.
     */
    public boolean isDisplayConfig() {
        return displayConfig;
    }

    /**
     * <p>Setter for the field <code>displayConfig</code>.</p>
     *
     * @param displayConfig a boolean.
     */
    public void setDisplayConfig(boolean displayConfig) {
        this.displayConfig = displayConfig;
    }

    /**
     * <p>isListConfig.</p>
     *
     * @return a boolean.
     */
    public boolean isListConfig() {
        return listConfig;
    }

    /**
     * <p>Setter for the field <code>listConfig</code>.</p>
     *
     * @param listConfig a boolean.
     */
    public void setListConfig(boolean listConfig) {
        this.listConfig = listConfig;
    }

    /**
     * <p>listAllConfigurations.</p>
     *
     * @return a {@link net.ihe.gazelle.common.filter.FilterDataModel} object.
     */
    public FilterDataModel<SimulatorResponderConfiguration> listAllConfigurations() {
        return new FilterDataModel<SimulatorResponderConfiguration>(getFilter()) {
            @Override
            protected Object getId(SimulatorResponderConfiguration simulatorResponderConfiguration) {
                return simulatorResponderConfiguration.getId();
            }
        };
    }

    /**
     * <p>createNewConfiguration.</p>
     */
    public void createNewConfiguration() {
        selectedConfiguration = null;
        selectedConfiguration = new SimulatorResponderConfiguration();
        selectedConfiguration.setHl7Protocol(HL7Protocol.MLLP);
        editConfiguration();
    }

    /**
     * <p>copyConfiguration.</p>
     *
     * @param configuration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public void copyConfiguration(SimulatorResponderConfiguration configuration) {
        selectedConfiguration = new SimulatorResponderConfiguration(configuration);
        editConfiguration();
    }

    /**
     * <p>editConfiguration.</p>
     */
    public void editConfiguration() {
        editConfig = true;
        displayConfig = false;
        listConfig = false;
    }

    /**
     * <p>editConfiguration.</p>
     *
     * @param configuration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public void editConfiguration(SimulatorResponderConfiguration configuration) {
        selectedConfiguration = configuration;
        editConfiguration();
    }

    /**
     * <p>displayConfiguration.</p>
     */
    public void displayConfiguration() {
        editConfig = false;
        displayConfig = true;
        listConfig = false;
    }

    /**
     * <p>displayConfiguration.</p>
     *
     * @param configuration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public void displayConfiguration(SimulatorResponderConfiguration configuration) {
        selectedConfiguration = configuration;
        displayConfiguration();
    }

    /**
     * <p>displayList.</p>
     */
    public void displayList() {
        editConfig = false;
        displayConfig = false;
        listConfig = true;
        selectedConfiguration = null;
    }

    /**
     * <p>saveConfiguration.</p>
     */
    public void saveConfiguration() {
        try {
            selectedConfiguration = selectedConfiguration.save(null);
            FacesMessages.instance().addFromResourceBundle("gazelle.HL7Common.configurationSaved");
            displayList();
        } catch (BatchUpdateException e) {
            e.printStackTrace();
            FacesMessages.instance().addFromResourceBundle("gazelle.HL7Common.configurationCannotBeSaved",
                    e.getMessage());
        } catch (ConstraintViolationException e) {
            e.printStackTrace();
            FacesMessages.instance().addFromResourceBundle("gazelle.HL7Common.configurationCannotBeSaved",
                    e.getMessage());
        }
    }

    /**
     * <p>stopResponder.</p>
     *
     * @param configuration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public void stopResponder(SimulatorResponderConfiguration configuration) {
        if (configuration != null) {
            ServerLocal server = (ServerLocal) Component.getInstance(configuration.getServerBeanName());
            boolean stopped = server.stopServer(configuration.getName());
            if (!stopped) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,"The system has not been able to stop this server");
            }
            configuration.setRunning(!stopped);
            try {
                configuration.save(null);
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.WARN,"Unable to save the new state of this server");
            }
        }
    }

    /**
     * <p>startResponder.</p>
     *
     * @param configuration a {@link net.ihe.gazelle.HL7Common.model.SimulatorResponderConfiguration} object.
     */
    public void startResponder(SimulatorResponderConfiguration configuration) {
        if (configuration != null) {
            if (configuration.getServerBeanName() == null || configuration.getServerBeanName().isEmpty()) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,"No Server Bean Name has been defined for this server !");
                return;
            } else {
                ServerLocal server = (ServerLocal) Component.getInstance(configuration.getServerBeanName());
                if (server != null) {
                    boolean running = server.startServer(configuration.getName());
                    if (!running) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR,"The system has not been able to start this server");
                    }
                    configuration.setRunning(running);
                    try {
                        configuration.save(null);
                    } catch (Exception e) {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR,"Unable to save the new state of this server");
                    }
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,configuration.getServerBeanName() + " is not an available bean");
                }
            }
        }
    }


    /**
     * <p>getListOfServerBean.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getListOfServerBean() {
        SimulatorResponderConfigurationQuery query = new SimulatorResponderConfigurationQuery();
        List<String> beanNames = query.serverBeanName().getListDistinct();
        if (beanNames == null || beanNames.isEmpty()) {
            return null;
        } else {
            return beanNames;
        }
    }

    /**
     * <p>getHl7Protocols.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<SelectItem> getHl7Protocols() {
        return HL7Protocol.getListForSelectMenu();
    }

    /**
     * <p>getEncodings.</p>
     *
     * @return an array of {@link net.ihe.gazelle.hl7.validator.client.MessageEncoding} objects.
     */
    public MessageEncoding[] getEncodings() {
        return MessageEncoding.values();
    }
}
