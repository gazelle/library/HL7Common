package net.ihe.gazelle.HL7Common.exception;

import ca.uhn.hl7v2.AcknowledgmentCode;
import net.ihe.gazelle.HL7Common.utils.HL7Constants;

/**
 * <p>MissingRequiredSegmentException class.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */

public class MissingRequiredSegmentException extends HL7v2ParsingException {


    public MissingRequiredSegmentException(String inMissingSegment, String errorLocation) {
        super(inMissingSegment, errorLocation);
    }

    @Override
    public String getHL7ErrorCode(){
       return HL7Constants.SEGMENT_SEQUENCE_ERROR;
    }

    @Override
    public String getErrorMessage(){
        return "Missing required segment: " + getElementInError();
    }

    @Override
    public AcknowledgmentCode getAckCode() {
        return AcknowledgmentCode.AR;
    }
}
