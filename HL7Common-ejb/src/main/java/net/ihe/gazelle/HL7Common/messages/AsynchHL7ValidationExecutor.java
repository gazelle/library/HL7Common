package net.ihe.gazelle.HL7Common.messages;

import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.DependsOn;

/**
 * <p>HL7ValidationExecutor class.</p>
 *
 * @author aberge
 * @version 1.0: 05/10/17
 */
@Name("asynchHL7ValidationExecutor")
@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Scope(ScopeType.APPLICATION)
@GenerateInterface("IAsynchHL7ValidationExecutor")
public class AsynchHL7ValidationExecutor implements IAsynchHL7ValidationExecutor {

    private static final Logger LOG = LoggerFactory.getLogger(AsynchHL7ValidationExecutor.class);
    private static final String OBSERVER_NAME = "validateHL7MessageAsync";

    @Observer(OBSERVER_NAME)
    @Transactional
    public void validateMessages(TransactionInstance transactionInstance) {
        HL7Validator.validateTransactionInstance(transactionInstance);
    }

    public static String getObserverName(){
        return OBSERVER_NAME;
    }
}
