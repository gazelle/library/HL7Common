package net.ihe.gazelle.HL7Common.exception;


import ca.uhn.hl7v2.AcknowledgmentCode;
import net.ihe.gazelle.HL7Common.utils.HL7Constants;

/**
 * <p>UnexpectedMessageType class.</p>
 *
 * @author abe
 * @version 1.0: 15/04/19
 */

public class UnsupportedTriggerEventException extends HL7v2ParsingException {

    private static final String TRIGGER_EVENT_LOCATION = "MSH^1^9^0^2";

    public UnsupportedTriggerEventException() {
        super((String) null, TRIGGER_EVENT_LOCATION);
    }

    @Override
    public String getHL7ErrorCode() {
        return HL7Constants.UNSUPPORTED_TRIGGER_EVENT_HL7_CODE;
    }

    @Override
    public String getErrorMessage() {
        return "Trigger event not supported";
    }

    @Override
    public AcknowledgmentCode getAckCode() {
        return AcknowledgmentCode.AR;
    }
}
