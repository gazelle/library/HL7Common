package net.ihe.gazelle.HL7Common.responder;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;

import java.io.IOException;
import java.util.Map;

/**
 * <b>Class Description : </b>DummyHL7Responder<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 29/08/16
 */
public class DummyHL7Responder extends IHEDefaultHandler {
    /**
     * {@inheritDoc}
     *
     * Create a new instance of IHEDefaultHandler from the current one
     */
    @Override
    public IHEDefaultHandler newInstance() {
        return new DummyHL7Responder();
    }

    /** {@inheritDoc} */
    @Override
    public Message processMessage(Message incomingMessage) throws ReceivingApplicationException, HL7Exception {
        try {
            return incomingMessage.generateACK();
        } catch (IOException e){
            throw new ReceivingApplicationException("Unable to generate default acknowledgement");
        }
    }

    @Override
    public Message processMessage(final Message incomingMessage, Map<String, Object> inMetadata)
            throws ReceivingApplicationException, HL7Exception {
        return processMessage(incomingMessage);
    }
}
