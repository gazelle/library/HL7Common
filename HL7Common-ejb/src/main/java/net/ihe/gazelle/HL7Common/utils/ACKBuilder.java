package net.ihe.gazelle.HL7Common.utils;

import ca.uhn.hl7v2.AcknowledgmentCode;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.ACK;
import ca.uhn.hl7v2.model.v25.segment.ERR;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.exception.HL7v2ParsingException;
import net.ihe.gazelle.HL7Common.exception.ReceivingApplicationUnavailableException;
import net.ihe.gazelle.HL7Common.exception.ReceivingFacilityUnavailableException;
import net.ihe.gazelle.HL7Common.exception.UnsupportedMessageTypeException;
import net.ihe.gazelle.HL7Common.exception.UnsupportedTriggerEventException;
import net.ihe.gazelle.HL7Common.messages.SegmentBuilder;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * <p>Receiver class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
public class ACKBuilder {

    private static Log log = Logging.getLog(ACKBuilder.class);

    private String expectedReceivingApplication;
    private String expectedReceivingFacility;
    private List<String> acceptedMessageTypes;
    private List<String> acceptedEvents;
    private String event;
    private String receivingApplication;
    private String receivingFacility;
    private String messageType;

    public ACKBuilder(String inExpectedReceivingApplication, String inExpectedReceivingFacility,
                      List<String> inAcceptedTypes, List<String> inAcceptedEvents) {
        this.expectedReceivingApplication = inExpectedReceivingApplication;
        this.expectedReceivingFacility = inExpectedReceivingFacility;
        this.acceptedEvents = inAcceptedEvents;
        this.acceptedMessageTypes = inAcceptedTypes;
    }

    /**
     * Does not filter on Receiving application/facility values
     *
     * @param inAcceptedTypes
     * @param inAcceptedEvents
     */
    public ACKBuilder(List<String> inAcceptedTypes, List<String> inAcceptedEvents) {
        this.expectedReceivingApplication = null;
        this.expectedReceivingFacility = null;
        this.acceptedEvents = inAcceptedEvents;
        this.acceptedMessageTypes = inAcceptedTypes;
    }

    /**
     * Check the MSH segment values and creates an NACK if necessary
     *
     * @param receivedMessage a {@link Message} object.
     *
     * @return null if the message can be process
     *
     * @throws HL7Exception if any.
     */
    public void isMessageAccepted(Message receivedMessage) throws HL7v2ParsingException {
        Terser t = new Terser(receivedMessage);
        try {
            receivingApplication = t.get("/.MSH-5-1");
            receivingFacility = t.get("/.MSH-6-1");
            messageType = t.get("/.MSH-9-1");
            event = t.get("/.MSH-9-2");
        } catch (HL7Exception e) {
            throw new HL7v2ParsingException(e);
        }
        // check receivingApplication and receivingFacility are those expected
        if ((receivingApplication != null) && !receivingApplication.equals(expectedReceivingApplication)) {
            throw new ReceivingApplicationUnavailableException(receivingApplication);
        }
        if ((receivingFacility != null) && !receivingFacility.equals(expectedReceivingFacility)) {
            throw new ReceivingFacilityUnavailableException(receivingFacility);
        }

        // check message type and trigger event are accepted

        if ((messageType != null) && !acceptedMessageTypes.contains(messageType)) {
            throw new UnsupportedMessageTypeException();
        }

        if ((event != null) && !acceptedEvents.contains(event)) {
            throw new UnsupportedTriggerEventException();
        }
    }

    /**
     * <p>buildNack.</p>
     *
     *
     * @return the acknowledgement reporting the error
     */
    public Message buildNack(Message incomingMessage, HL7v2ParsingException exception) {
        ACK ack = buildAck(incomingMessage, exception.getAckCode());
        fillERRSegment(ack.getERR(), exception.getErrorLocation(), exception.getHL7ErrorCode(), exception
                .getErrorMessage());
        return ack;
    }

    /**
     *
     * @param incomingMessage
     * @param ackCode
     * @return
     */
    public ACK buildAck(Message incomingMessage, AcknowledgmentCode ackCode) {
        Terser t = new Terser(incomingMessage);
        String sendingApplication;
        String sendingFacility;
        String messageControlId;
        try {
            sendingApplication = t.get("/.MSH-3-1");
            sendingFacility = t.get("/.MSH-4-1");
            String versionId = t.get("/.MSH-12-1");
            String charset = t.get("/.MSH-18");
            messageControlId = t.get("/.MSH-10");
            ACK ack = new ACK();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            SegmentBuilder.fillMSHSegment(ack.getMSH(), sendingApplication, sendingFacility, receivingApplication,
                    receivingFacility, "ACK", event, "ACK", versionId, sdf, charset);
            SegmentBuilder.fillMSASegment(ack.getMSA(), messageControlId, ackCode);
            return ack;
        } catch (HL7Exception e) {
            log.error("Cannot extract data from incoming message");
            return null;
        }
    }

    public ACK buildAck(Message incomingMessage){
        return buildAck(incomingMessage, AcknowledgmentCode.AA);
    }

    /**
     * @param errSegment
     * @param errorLocation
     * @param hl7ErrorCode
     * @param userMessage
     */
    private static void fillERRSegment(ERR errSegment, String errorLocation, String hl7ErrorCode,
                                       String userMessage) {
        try {
            if (errorLocation != null) {
                errSegment.getErr2_ErrorLocation(0).parse(errorLocation);
            }
            errSegment.getErr3_HL7ErrorCode().getIdentifier().setValue(hl7ErrorCode);
            errSegment.getErr4_Severity().setValue(HL7Constants.SEVERITY_ERROR);
            errSegment.getErr8_UserMessage().setValue(userMessage);
        } catch (HL7Exception e) {
            log.error(e.getMessage(), e);
        }
    }

}
