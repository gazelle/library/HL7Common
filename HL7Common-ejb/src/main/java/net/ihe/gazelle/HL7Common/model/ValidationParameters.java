package net.ihe.gazelle.HL7Common.model;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.model.ApplicationConfiguration;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
/**
 * <p>ValidationParameters class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Name("validation_parameters")
@Table(name = "validation_parameters", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "validation_parameters_sequence", sequenceName = "ort_validation_parameters_id_seq", allocationSize = 1)
public class ValidationParameters implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(generator = "validation_parameters_sequence", strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "message_type")
    @NotNull
    private String messageType;

    @Column(name = "profile_oid")
    @NotNull
    private String profileOID;

    @ManyToOne
    @NotNull
    private Transaction transaction;

    @ManyToOne
    @NotNull
    private Actor actor;

    @ManyToOne
    private Domain domain;

    @Column(name = "java_package")
    private String javaPackage;

    @Column(name = "validator_type")
    @Enumerated(EnumType.STRING)
    private ValidatorType validatorType;


    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a int.
     */
    public int getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a int.
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>messageType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessageType() {
        return messageType;
    }

    /**
     * <p>Setter for the field <code>messageType</code>.</p>
     *
     * @param messageType a {@link java.lang.String} object.
     */
    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }


    /**
     * <p>Getter for the field <code>profileOID</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProfileOID() {
        return profileOID;
    }

    /**
     * <p>Setter for the field <code>profileOID</code>.</p>
     *
     * @param profileOID a {@link java.lang.String} object.
     */
    public void setProfileOID(String profileOID) {
        this.profileOID = profileOID;
    }

    /**
     * <p>Getter for the field <code>transaction</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public Transaction getTransaction() {
        return transaction;
    }

    /**
     * <p>Setter for the field <code>transaction</code>.</p>
     *
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    /**
     * <p>Getter for the field <code>actor</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Actor getActor() {
        return actor;
    }

    /**
     * <p>Setter for the field <code>actor</code>.</p>
     *
     * @param actor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public void setActor(Actor actor) {
        this.actor = actor;
    }

    /**
     * <p>Constructor for ValidationParameters.</p>
     */
    public ValidationParameters() {

    }

    /**
     * <p>getPermanentLinkToProfile.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPermanentLinkToProfile() {
        String baseUrlForProfiles = ApplicationConfiguration.getValueOfVariable("gazelle_hl7v2_validator_url");
        return baseUrlForProfiles.concat("/viewProfile.seam?oid=" + this.profileOID);
    }

    /**
     * <p>Constructor for ValidationParameters.</p>
     *
     * @param messageType a {@link java.lang.String} object.
     * @param profileOID a {@link java.lang.String} object.
     * @param actor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     */
    public ValidationParameters(String messageType,
                                String profileOID, Actor actor, Transaction transaction) {
        this.messageType = messageType;
        this.profileOID = profileOID;
        this.actor = actor;
        this.transaction = transaction;
    }

    /**
     * <p>Getter for the field <code>javaPackage</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getJavaPackage() {
        if (this.javaPackage != null && !this.javaPackage.isEmpty()) return this.javaPackage;
        else {
            String validatorUrl = ApplicationConfiguration.getValueOfVariable("gazelle_hl7v2_validator_url");
            ClientRequest request = new ClientRequest(validatorUrl.concat("/rest/GetPackageForProfile"));
            request.queryParameter("oid", this.profileOID);
            try {
                ClientResponse<String> response = request.get(String.class);
                if (response.getStatus() == 200) {
                    this.javaPackage = response.getEntity();
                    EntityManager entityManager = EntityManagerService.provideEntityManager();
                    entityManager.merge(this);
                    entityManager.flush();
                    return this.javaPackage;
                } else
                    return null;
            } catch (Exception e) {
                this.javaPackage = null;
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * <p>Getter for the field <code>validatorType</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.ValidatorType} object.
     */
    public ValidatorType getValidatorType() {
        return validatorType;
    }

    /**
     * <p>Setter for the field <code>validatorType</code>.</p>
     *
     * @param validatorType a {@link net.ihe.gazelle.HL7Common.model.ValidatorType} object.
     */
    public void setValidatorType(ValidatorType validatorType) {
        this.validatorType = validatorType;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((messageType == null) ? 0 : messageType.hashCode());
        result = prime * result
                + ((profileOID == null) ? 0 : profileOID.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ValidationParameters other = (ValidationParameters) obj;
        if (messageType == null) {
            if (other.messageType != null)
                return false;
        } else if (!messageType.equals(other.messageType))
            return false;
        if (profileOID == null) {
            if (other.profileOID != null)
                return false;
        } else if (!profileOID.equals(other.profileOID))
            return false;
        return true;
    }

    /**
     * <p>Setter for the field <code>domain</code>.</p>
     *
     * @param domain a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    /**
     * <p>Getter for the field <code>domain</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public Domain getDomain() {
        return domain;
    }

    /**
     * <p>Setter for the field <code>javaPackage</code>.</p>
     *
     * @param javaPackage a {@link java.lang.String} object.
     */
    public void setJavaPackage(String javaPackage) {
        this.javaPackage = javaPackage;
    }

}
