package net.ihe.gazelle.HL7Common.messages;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.util.Terser;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

/**
 * Class Description <b>HL7MessageDecoder</b>
 * This class is aimed to extract some values from the received HL7 messages
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes Bretagne Atlantique
 * @version 1.0 - 2011, July 18th
 */
public class HL7MessageDecoder {

    private static Log log = Logging.getLog(HL7MessageDecoder.class);



    /**
     * Returns the message type of the message formatted as messageType^triggerEvent^messageStructure
     *
     * @param msg a {@link ca.uhn.hl7v2.model.Message} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getMessageType(Message msg) {
        if (msg == null) {
            return null;
        } else {
            try {
                String value;
                Terser terser = new Terser(msg);
                StringBuffer messageType = new StringBuffer();
                value = terser.get("/.MSH-9-1");
                if (value != null && !value.isEmpty()) {
                    messageType.append(value);
                }
                value = terser.get("/.MSH-9-2");
                if (value != null && !value.isEmpty()) {
                    messageType.append('^');
                    messageType.append(value);
                }
                value = terser.get("/.MSH-9-3");
                if (value != null && !value.isEmpty()) {
                    messageType.append('^');
                    messageType.append(value);
                }
                return messageType.toString();
            } catch (HL7Exception e) {
                log.error("unable to extract the message type from MSH segment");
                return null;
            }
        }
    }


    /**
     * Returns the message type of the message formatted as messageType^triggerEvent^messageStructure
     *
     * @param msg a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getMessageType(String msg) {
        String messageType;
        if (msg == null) {
            return null;
        } else {
            int mshBegin = msg.indexOf("|");
            for (int i = 0; i < 7; i++) {
                mshBegin = msg.indexOf("|", mshBegin + 1);
            }

            int mshEnd = msg.indexOf("|", mshBegin + 1);
            messageType = msg.substring(mshBegin + 1, mshEnd);

            return messageType;
        }
    }

    /**
     * <p>getSendingApplication.</p>
     *
     * @param msg a {@link ca.uhn.hl7v2.model.Message} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getSendingApplication(Message msg) {
        if (msg == null) {
            return null;
        } else {
            try {
                String value = "";
                String sendingApplication = "";
                Terser terser = new Terser(msg);

                value = terser.get("/.MSH-3-1");
                sendingApplication = value;

                value = terser.get("/.MSH-3-2");
                if (value != null && !value.isEmpty()) {
                    sendingApplication = sendingApplication + "^" + value;

                    value = terser.get("/.MSH-3-3");
                    if (value != null && !value.isEmpty()) {
                        sendingApplication = sendingApplication + "^" + value;
                    }
                } else {
                    value = terser.get("/.MSH-3-3");
                    if (value != null && !value.isEmpty()) {
                        sendingApplication = sendingApplication + "^^" + value;
                    }
                }

                return sendingApplication;

            } catch (HL7Exception e) {
                log.error("unable to extract the sending Application from MSH segment");
                return null;
            }
        }
    }


    /**
     * <p>getSendingfacility.</p>
     *
     * @param msg a {@link ca.uhn.hl7v2.model.Message} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getSendingfacility(Message msg) {
        if (msg == null) {
            return null;
        } else {
            try {
                String value = "";
                String sendingfacility = "";
                Terser terser = new Terser(msg);

                value = terser.get("/.MSH-4-1");
                sendingfacility = value;

                value = terser.get("/.MSH-4-2");
                if (value != null && !value.isEmpty()) {
                    sendingfacility = sendingfacility + "^" + value;

                    value = terser.get("/.MSH-4-3");
                    if (value != null && !value.isEmpty()) {
                        sendingfacility = sendingfacility + "^" + value;
                    }
                } else {
                    value = terser.get("/.MSH-4-3");
                    if (value != null && !value.isEmpty()) {
                        sendingfacility = sendingfacility + "^^" + value;
                    }
                }

                return sendingfacility;

            } catch (HL7Exception e) {
                log.error("unable to extract the sending facility from MSH segment");
                return null;
            }
        }
    }


    /**
     * <p>getReceivingApplication.</p>
     *
     * @param msg a {@link ca.uhn.hl7v2.model.Message} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getReceivingApplication(Message msg) {
        if (msg == null) {
            return null;
        } else {
            try {
                String value = "";
                String receivingApplication = "";
                Terser terser = new Terser(msg);

                value = terser.get("/.MSH-5-1");
                receivingApplication = value;

                value = terser.get("/.MSH-5-2");
                if (value != null && !value.isEmpty()) {
                    receivingApplication = receivingApplication + "^" + value;

                    value = terser.get("/.MSH-5-3");
                    if (value != null && !value.isEmpty()) {
                        receivingApplication = receivingApplication + "^" + value;
                    }
                } else {
                    value = terser.get("/.MSH-5-3");
                    if (value != null && !value.isEmpty()) {
                        receivingApplication = receivingApplication + "^^" + value;
                    }
                }

                return receivingApplication;

            } catch (HL7Exception e) {
                log.error("unable to extract the receiving Application from MSH segment");
                return null;
            }
        }
    }


    /**
     * <p>getReceivingFacility.</p>
     *
     * @param msg a {@link ca.uhn.hl7v2.model.Message} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getReceivingFacility(Message msg) {
        if (msg == null) {
            return null;
        } else {
            try {
                String value = "";
                String receivingFacility = "";
                Terser terser = new Terser(msg);

                value = terser.get("/.MSH-6-1");
                receivingFacility = value;

                value = terser.get("/.MSH-6-2");
                if (value != null && !value.isEmpty()) {
                    receivingFacility = receivingFacility + "^" + value;

                    value = terser.get("/.MSH-6-3");
                    if (value != null && !value.isEmpty()) {
                        receivingFacility = receivingFacility + "^" + value;
                    }
                } else {
                    value = terser.get("/.MSH-6-3");
                    if (value != null && !value.isEmpty()) {
                        receivingFacility = receivingFacility + "^^" + value;
                    }
                }

                return receivingFacility;

            } catch (HL7Exception e) {
                log.error("unable to extract the receiving facility from MSH segment");
                return null;
            }
        }
    }


    /**
     * <p>getCharacterSet.</p>
     *
     * @param msg a {@link ca.uhn.hl7v2.model.Message} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getCharacterSet(Message msg) {
        if (msg == null) {
            return null;
        } else {
            try {
                String characterSet = "";
                Terser terser = new Terser(msg);

                characterSet = terser.get("/.MSH-18");

                return characterSet;

            } catch (HL7Exception e) {
                log.error("unable to extract the characterSet from MSH segment");
                return null;
            }
        }
    }


}
