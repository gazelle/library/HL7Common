package net.ihe.gazelle.HL7Common.responder;

import net.ihe.gazelle.simulator.common.tf.model.Actor;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Startup;
import org.jboss.seam.annotations.Transactional;

import javax.ejb.DependsOn;

/**
 * <b>Class Description : </b>DummyHL7Server<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 29/08/16
 */

@Startup(depends = {"entityManager"})
@DependsOn({"entityManager"})
@Name("dummyHL7Server")
@Scope(ScopeType.APPLICATION)
public class DummyHL7Server extends AbstractServer<DummyHL7Responder> {
    /**
     * {@inheritDoc}
     *
     * This method is called at start up
     */
    @Override
    @Create
    @Transactional
    public void startServers() {
        Actor simulatedActor = Actor.findActorWithKeyword("UNKNOWN", entityManager);
        handler = new DummyHL7Responder();
        startServers(simulatedActor, null, null);
    }
}
