package net.ihe.gazelle.HL7Common.initiator;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import net.ihe.gazelle.HL7Common.messages.HL7MessageDecoder;
import net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration;
import net.ihe.gazelle.HL7Common.utils.HL7MessageUtils;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.log.Log;
import org.jboss.seam.log.Logging;

import javax.persistence.EntityManager;
import java.util.Date;

/**
 * Class description : <b> Initiator </b> This class is used to send HL7v2 messages and store the request and the response in a HL7Message object if needed it can be extended
 *
 * @author Anne-Gaëlle Bergé / INRIA Rennes Bretagne Atlantique
 * @version 1.0 - 2011, July 18th
 */
public class Initiator {

    private HL7V2ResponderSUTConfiguration sut;
    private String sendingFacility;
    private String sendingApplication;
    private Actor simulatedActor;
    private Actor sutActor;
    private String messageStringToSend;
    private String messageType;
    private Transaction transaction;
    private Domain domain;
    private EntityManager entityManager = null;
    private String domainKeyword;
    private Parser parser;

    private static Log log = Logging.getLog(Initiator.class);

    /**
     * <p>Constructor for Initiator.</p>
     *
     * @param sutConfiguration a {@link net.ihe.gazelle.HL7Common.model.HL7V2ResponderSUTConfiguration} object.
     * @param sendingFacility a {@link java.lang.String} object.
     * @param sendingApplication a {@link java.lang.String} object.
     * @param simulatedActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     * @param transaction a {@link net.ihe.gazelle.simulator.common.tf.model.Transaction} object.
     * @param messageStringToSend a {@link java.lang.String} object.
     * @param messageType a {@link java.lang.String} object.
     * @param domainKeyword a {@link java.lang.String} object.
     * @param sutActor a {@link net.ihe.gazelle.simulator.common.tf.model.Actor} object.
     */
    public Initiator(HL7V2ResponderSUTConfiguration sutConfiguration, String sendingFacility, String sendingApplication,
                     Actor simulatedActor, Transaction transaction, String messageStringToSend, String messageType,
                     String domainKeyword, Actor sutActor) {
        this.sut = sutConfiguration;
        this.sendingFacility = sendingFacility;
        this.sendingApplication = sendingApplication;
        this.simulatedActor = simulatedActor;
        this.messageStringToSend = messageStringToSend;
        this.messageType = messageType;
        this.transaction = transaction;
        this.domainKeyword = domainKeyword;
        this.sutActor = sutActor;
    }

    /**
     * <p>sendMessage.</p>
     *
     * @return the HL7 message Data Model with the unique send message.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public TransactionInstance sendMessage() throws HL7Exception {
        Message msg = parseMessage(messageStringToSend, sut.getMessageEncoding());
        if (msg != null) {
            Message response = null;
            try {
                switch (sut.getHl7Protocol()) {
                    case MLLP:
                        response = GazelleHL7v2Client.sendMessageOverMllp(msg, sut, null);
                        break;
                    case HTTP:
                        response = GazelleHL7v2Client.sendMessageOverHttp(msg, sut, null);
                        break;
                    default:
                        response = null;
                        log.error(sut.getHl7Protocol().getValue() + " is not implemented");
                        break;
                }
                String responseString = null;
                String responseType = null;
                if (response != null) {
                    responseString = parser.encode(response);
                    if (responseString != null && responseString.isEmpty()) {
                        responseString = null;
                    } else {
                        responseType = HL7MessageDecoder.getMessageType(response);
                    }
                }
                TransactionInstance transactionInstance = TransactionInstanceUtil.newInstance(sendingApplication, sendingFacility, sut.getApplication(),
                        sut.getFacility(), messageStringToSend, responseString,
                        new Date(), transaction, messageType, responseType, simulatedActor, simulatedActor,
                        sutActor, getDomain(), null);
                transactionInstance = transactionInstance.save(entityManager);
                TransactionInstanceUtil transactionInstanceUtil = new TransactionInstanceUtil();
                transactionInstanceUtil.ExtractIds(msg,response,transactionInstance);
               return transactionInstance;
            } catch (HL7Exception e) {
                log.error(e.getMessage(), e);
                throw new HL7Exception(e.getMessage());
            }
        } else {
            throw new HL7Exception("Generated message is not parsable, unable to send it!");
        }
    }

    /**
     * <p>sendMessageAndGetTheHL7Message.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public TransactionInstance sendMessageAndGetTheHL7Message() throws HL7Exception {
        Message parsedMessage = parseMessage(messageStringToSend, sut.getMessageEncoding());
        if (parsedMessage != null) {
            Message response = null;
            try {
                switch (sut.getHl7Protocol()) {
                    case MLLP:
                        response = GazelleHL7v2Client.sendMessageOverMllp(parsedMessage, sut, getEntityManager());
                        break;
                    case HTTP:
                        response = GazelleHL7v2Client.sendMessageOverHttp(parsedMessage, sut, getEntityManager());
                        break;
                    default:
                        response = null;
                        log.error(sut.getHl7Protocol().getValue() + " is not implemented");
                        break;
                }
                String responseString = null;
                String responseType = null;
                if (response != null) {
                    PipeParser pipe = new PipeParser();
                    responseString = pipe.encode(response);
                    if (responseString != null && responseString.isEmpty()) {
                        responseString = null;
                    } else {
                        responseType = HL7MessageDecoder.getMessageType(response);
                    }
                }
                TransactionInstance transactionInstance = TransactionInstanceUtil.newInstance(sendingApplication, sendingFacility, sut.getApplication(),
                        sut.getFacility(), messageStringToSend, responseString,
                        new Date(), transaction, messageType, responseType, simulatedActor, simulatedActor,
                        sutActor, getDomain(), null);

                transactionInstance = transactionInstance.save(entityManager);
                TransactionInstanceUtil transactionInstanceUtil = new TransactionInstanceUtil();
                transactionInstanceUtil.ExtractIds(parsedMessage,response,transactionInstance);
                return transactionInstance;

            } catch (HL7Exception e) {
                log.error(e.getMessage());
                throw new HL7Exception(e.getMessage(), e);
            }
        } else {
            throw new HL7Exception("Generated message is not parsable, unable to send it!");
        }
    }

    /**
     * <p>Getter for the field <code>entityManager</code>.</p>
     *
     * @return a {@link javax.persistence.EntityManager} object.
     */
    public EntityManager getEntityManager() {
        if (this.entityManager == null) {
            this.entityManager = EntityManagerService.provideEntityManager();
        }
        return entityManager;
    }

    /**
     * <p>Setter for the field <code>entityManager</code>.</p>
     *
     * @param entityManager a {@link javax.persistence.EntityManager} object.
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    /**
     * <p>Getter for the field <code>domain</code>.</p>
     *
     * @return a {@link net.ihe.gazelle.simulator.common.tf.model.Domain} object.
     */
    public Domain getDomain() {
        if (domain == null && domainKeyword != null) {
            this.domain = Domain.getDomainByKeyword(domainKeyword, getEntityManager());
        }
        return domain;
    }

    /**
     * <p>sendMessageAndGetResponse.</p>
     *
     * @return a {@link ca.uhn.hl7v2.model.Message} object.
     * @throws ca.uhn.hl7v2.HL7Exception if any.
     */
    public Message sendMessageAndGetResponse() throws HL7Exception {
        Message msg = parseMessage(messageStringToSend, sut.getMessageEncoding());
        if (msg != null) {
            Message response = null;
            switch (sut.getHl7Protocol()) {
                case MLLP:
                    response = GazelleHL7v2Client.sendMessageOverMllp(msg, sut, null);
                    break;
                case HTTP:
                    response = GazelleHL7v2Client.sendMessageOverHttp(msg, sut, null);
                    break;
                default:
                    throw new HL7Exception(sut.getHl7Protocol().getValue() + " is not implemented");
            }
            String responseString = parser.encode(response);
            String responseType = HL7MessageDecoder.getMessageType(response);
            TransactionInstance transactionInstance = TransactionInstanceUtil.newInstance(sendingApplication, sendingFacility, sut.getApplication(),
                    sut.getFacility(), messageStringToSend, responseString,
                    new Date(), transaction, messageType, responseType, simulatedActor, simulatedActor,
                    sutActor, getDomain(), null);
            transactionInstance = transactionInstance.save(entityManager);
            TransactionInstanceUtil.ExtractIds(msg,response,transactionInstance);
            return response;
        } else {
            throw new HL7Exception("Generated message is not parsable, unable to send it!");
        }
    }

    private Message parseMessage(String messageStringToSend, MessageEncoding sutEncoding){
        parser = HL7MessageUtils.getParserForMessageEncoding(sut.getMessageEncoding());
        parser.getParserConfiguration().setEncodeEmptyMandatoryFirstSegments(true);
        Message msg = null;
        if (messageStringToSend != null) {
            try {
                msg = parser.parse(messageStringToSend);
            } catch (Exception e) {
                log.error(e.getMessage() + ": " + messageStringToSend, e);
            }
        } else {
            log.error("The generated message is null, cannot parse it");
        }
        return msg;
    }
}
