package net.ihe.gazelle.HL7Common.utils;

/**
 * <p>HL7Constants class.</p>
 *
 * @author abe
 * @version 1.0: 16/04/19
 */

public class HL7Constants {

    private HL7Constants(){

    }

    /** Constant <code>UNSUPPORTED_MESSAGE_TYPE_HL7_CODE="200"</code> */
    public static final String UNSUPPORTED_MESSAGE_TYPE_HL7_CODE = "200";
    /** Constant <code>UNSUPPORTED_TRIGGER_EVENT_HL7_CODE="201"</code> */
    public static final String UNSUPPORTED_TRIGGER_EVENT_HL7_CODE = "201";
    /** Constant <code>DUPLICATE_KEY_IDENTIFIER="205"</code> */
    public static final String DUPLICATE_KEY_IDENTIFIER = "205";
    /** Constant <code>UNKNOWN_KEY_IDENTIFIER="204"</code> */
    public static final String UNKNOWN_KEY_IDENTIFIER = "204";
    /** Constant <code>APPLICATION_RECORD_LOCK="206"</code> */
    public static final String APPLICATION_RECORD_LOCK = "206";
    /** Constant <code>INTERNAL_ERROR="207"</code> */
    public static final String INTERNAL_ERROR = "207";
    /** Constant <code>REQUIRED_FIELD_MISSING="101"</code> */
    public static final String REQUIRED_FIELD_MISSING = "101";
    /** Constant <code>SEGMENT_SEQUENCE_ERROR="100"</code> */
    public static final String SEGMENT_SEQUENCE_ERROR = "100";
    /** Constant <code>SEVERITY_ERROR="E"</code> */
    public static final String SEVERITY_ERROR = "E";
    /** Constant <code>SEVERITY_WARNING="W"</code> */
    public static final String SEVERITY_WARNING = "W";
    /** Constant <code>SEVERITY_INFORMATION="I"</code> */
    public static final String SEVERITY_INFORMATION = "I";
    public static final String UNKNOWN_MESSAGE_TYPE = "1";
    public static final String UNKNOWN_TRIGGER_EVENT = "2";
}
