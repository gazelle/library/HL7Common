package net.ihe.gazelle.HL7Common.messages;

import net.ihe.gazelle.HL7Common.model.ValidationParameters;
import net.ihe.gazelle.HL7Common.utils.TransactionInstanceUtil;
import net.ihe.gazelle.hl7.validator.client.HL7v2Validator;
import net.ihe.gazelle.hl7.validator.client.ValidationContextBuilder;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.mb.validator.client.MBValidator;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>HL7Validator class.</p>
 *
 * @author aberge
 * @version 1.0: 05/10/17
 */

public class HL7Validator {

    private static final String UTF_8 = "UTF-8";
    private static final String VALIDATION_RESULTS_OVERVIEW = "ValidationResultsOverview";
    private static final String VALIDATION_TEST_RESULT = "ValidationTestResult";
    private static final String VALIDATION_REPORT = "ValidationReport";
    private static final String REPORT_HEADER = "ReportHeader";
    private static final String RESULT_OF_TEST = "ResultOfTest";
    private static final int FIRST_OCCURENCE = 0;

    private static final Logger LOG = LoggerFactory.getLogger(HL7Validator.class);

    public static TransactionInstance validateTransactionInstance(TransactionInstance instance) {
        TransactionInstance savedInstance = validateRequest(instance);
        return validateResponse(savedInstance);
    }

    public static TransactionInstance validateResponse(TransactionInstance instance) {
        ValidationParameters responseValidatorName = TransactionInstanceUtil.getValidatorForResponse(instance);
        validateMessage(instance.getResponse(), responseValidatorName);
        return saveTransactionInstance(instance);
    }

    public static TransactionInstance validateRequest(TransactionInstance instance){
        ValidationParameters requestValidatorName = TransactionInstanceUtil.getValidatorForRequest(instance);
        validateMessage(instance.getRequest(), requestValidatorName);
        return saveTransactionInstance(instance);
    }

    private static TransactionInstance saveTransactionInstance(TransactionInstance instance) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        return instance.save(entityManager);
    }


    public static void validateMessage(MessageInstance message, ValidationParameters validator) {
        if (validator != null){
            switch (validator.getValidatorType()){
                case V2:
                    validateHL7V2(message, validator);
                    break;
                case V3:
                    validateHL7V3(message, validator);
                    break;
                case FHIR:
                    validateFhir(message, validator);
                    break;
            }
        } else {
            message.setValidationStatus("NO VALIDATOR MATCHES");
        }
    }

    private static void validateHL7V3(MessageInstance instance, ValidationParameters validationParameters) {
       callMBVValidator(instance, validationParameters, "hl7v3_validator_url");
    }

    private static void callMBVValidator(MessageInstance instance, ValidationParameters validationParameters, String preferenceVariable){
        MBValidator validator;
        String endpoint = PreferenceService.getString(preferenceVariable);
        if (endpoint != null) {
            validator = new MBValidator(endpoint);
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, preferenceVariable + " is missing in app_configuration table");
            return;
        }
        if ((instance != null) && (instance.getContent() != null)) {
            String messageToValidate = instance.getContentAsString().replaceFirst("UTF", "UTF-");
            try {
                String validationResult = validator.validate(messageToValidate,
                        validationParameters.getProfileOID(), false);
                if (validationResult != null) {
                    instance.setValidationDetailedResult(validationResult.getBytes(Charset.forName(UTF_8)));
                    instance.setValidationStatus(getValidationResult(validationResult));
                }
            } catch (Exception e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
            }
        }
    }

    private static void validateHL7V2(MessageInstance instance, ValidationParameters validationParameters) {
        String endpoint = PreferenceService.getString("url_EVCS_ws");
        if (endpoint == null) {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The URL of the HL7v2 validation web service is not defined");
        } else {
            HL7v2Validator validator = new HL7v2Validator(endpoint, retrieveFaultLevels());
            validator.setXslLocation(PreferenceService.getString("xsl_location"));
            // request
            if (validationParameters != null) {
                try {
                    String result = validator.validate(instance.getContentAsString(), validationParameters.getProfileOID(), UTF_8);
                    if (result != null) {
                        instance.setValidationDetailedResult(result.getBytes(Charset.forName(UTF_8)));
                        instance.setValidationStatus(validator.getLastValidationStatus());
                    }
                } catch (Exception e) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, e.getMessage());
                }
            }
        }
    }

    private static Map<String, ValidationContextBuilder.FaultLevel> retrieveFaultLevels() {
        final Map<String, ValidationContextBuilder.FaultLevel> validationOptions = new HashMap<>();
        validationOptions.put(ValidationContextBuilder.LENGTH,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_length_fault_level")));
        validationOptions.put(ValidationContextBuilder.MESSAGE_STRUCTURE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_structure_fault_level")));
        validationOptions.put(ValidationContextBuilder.DATATYPE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_datatype_fault_level")));
        validationOptions.put(ValidationContextBuilder.DATAVALUE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_value_fault_level")));
        return validationOptions;
    }

    private static void validateFhir(MessageInstance fhirMessage, ValidationParameters validator) {
       callMBVValidator(fhirMessage, validator, "fhir_validation_url");
    }

    private static String getValidationResult(String inDetailedResult) {
        if (inDetailedResult == null || inDetailedResult.length() == FIRST_OCCURENCE) {
            return null;
        }
        Document document = buildDocumentFromString(inDetailedResult);
        try {
            if (document != null && inDetailedResult.contains(VALIDATION_RESULTS_OVERVIEW)) {
                return document.getRootElement().selectSingleNode(VALIDATION_RESULTS_OVERVIEW)
                        .selectSingleNode(VALIDATION_TEST_RESULT).getText();
            } else if (document != null && inDetailedResult.contains(VALIDATION_REPORT)) {
                return document.getRootElement().selectSingleNode(REPORT_HEADER).selectSingleNode(RESULT_OF_TEST)
                        .getText().toUpperCase();
            } else {
                return null;
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return null;
        }
    }



    private static Document buildDocumentFromString(String inString) {
        Document document;
        try {
            document = DocumentHelper.parseText(inString);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return null;
        }
        return document;
    }
}
