package net.ihe.gazelle.HL7Common.utils;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.HL7Common.model.ValidationParameters;
import net.ihe.gazelle.HL7Common.model.ValidationParametersQuery;
import net.ihe.gazelle.hl7.validator.client.MessageEncoding;
import net.ihe.gazelle.simulator.common.tf.model.Actor;
import net.ihe.gazelle.simulator.common.tf.model.Domain;
import net.ihe.gazelle.simulator.common.tf.model.Transaction;
import net.ihe.gazelle.simulator.message.model.EStandard;
import net.ihe.gazelle.simulator.message.model.MessageInstance;
import net.ihe.gazelle.simulator.message.model.MessageInstanceMetadata;
import net.ihe.gazelle.simulator.message.model.TransactionInstance;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * <b>Class Description : </b>TransactionInstanceUtil<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 07/09/15
 */
public class TransactionInstanceUtil {

    private static final String MSH_10_PATH = "/.MSH-10";
    private static final Logger LOG = LoggerFactory.getLogger(TransactionInstanceUtil.class);

    /**
     * <p>newInstance.</p>
     *
     * @param sendingApplication     a {@link String} object.
     * @param sendingFacility        a {@link String} object.
     * @param receivingApplication   a {@link String} object.
     * @param receivingFacility      a {@link String} object.
     * @param sentMessageContent     a {@link String} object.
     * @param receivedMessageContent a {@link String} object.
     * @param timeStamp              a {@link Date} object.
     * @param transaction            a {@link Transaction} object.
     * @param sentMessageType        a {@link String} object.
     * @param ackMessageType         a {@link String} object.
     * @param simulatedActor         a {@link Actor} object.
     * @param sendingActor           a {@link Actor} object.
     * @param receivingActor         a {@link Actor} object.
     * @param inDomain               a {@link Domain} object.
     *
     * @param senderIPAddress
     * @return a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     */
    public static TransactionInstance newInstance(String sendingApplication, String sendingFacility, String receivingApplication,
                                                  String receivingFacility, String sentMessageContent, String receivedMessageContent,
                                                  Date timeStamp, Transaction transaction, String sentMessageType, String ackMessageType,
                                                  Actor simulatedActor, Actor sendingActor, Actor receivingActor, Domain inDomain, String senderIPAddress) {
        TransactionInstance transactionInstance = new TransactionInstance();
        transactionInstance.setDomain(inDomain);
        transactionInstance.setTransaction(transaction);
        transactionInstance.setSimulatedActor(simulatedActor);
        transactionInstance.setTimestamp(timeStamp);
        transactionInstance.getRequest().setIssuingActor(sendingActor);
        transactionInstance.getRequest().setIssuer(sendingFacility + "/" + sendingApplication);
        transactionInstance.getRequest().setContent(sentMessageContent);
        transactionInstance.getResponse().setIssuingActor(receivingActor);
        transactionInstance.getResponse().setIssuer(receivingFacility + "/" + receivingApplication);
        transactionInstance.getRequest().setIssuerIpAddress(senderIPAddress);
        // TODO IP address in addition to the rec facility/application ?
        transactionInstance.getResponse().setContent(receivedMessageContent);
        transactionInstance.getResponse().setType(ackMessageType);
        transactionInstance.getRequest().setType(sentMessageType);
        transactionInstance.setStandard(EStandard.HL7V2);
        if (Contexts.isSessionContextActive() && Identity.instance().isLoggedIn()) {
            transactionInstance.setCompanyKeyword(Identity.instance().getCredentials().getUsername());
        }
        return transactionInstance;
    }

    /**
     * <p>getValidatorForRequest.</p>
     *
     * @param transactionInstance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.ValidationParameters} object.
     */
    public static ValidationParameters getValidatorForRequest(TransactionInstance transactionInstance) {
        if (transactionInstance != null) {
            MessageInstance request = transactionInstance.getRequest();
            ValidationParametersQuery query = new ValidationParametersQuery();
            query.actor().keyword().eq(request.getIssuingActor().getKeyword());
            query.transaction().keyword().eq(transactionInstance.getTransaction().getKeyword());
            query.messageType().eq(request.getType());
            query.domain().keyword().eq(transactionInstance.getDomain().getKeyword());
            return query.getUniqueResult();
        } else {
            return null;
        }
    }

    /**
     * <p>getValidatorForResponse.</p>
     *
     * @param transactionInstance a {@link net.ihe.gazelle.simulator.message.model.TransactionInstance} object.
     *
     * @return a {@link net.ihe.gazelle.HL7Common.model.ValidationParameters} object.
     */
    public static ValidationParameters getValidatorForResponse(TransactionInstance transactionInstance) {
        MessageInstance response = transactionInstance.getResponse();
        String responseMessageType = response.getType();
        ValidationParametersQuery query = buildQueryForResponse(transactionInstance, response);
        query.messageType().eq(response.getType());
        ValidationParameters validationParameters = query.getUniqueResult();
        if (validationParameters == null && responseMessageType.contains("ACK")){
            //HLSEVENCMN-79: When the simulator does not find a match for the message profile, it should not use the ACK, it might not be accurate
            // match with ACK only if message type contains ACK
            ValidationParametersQuery newQuery = buildQueryForResponse(transactionInstance, response);
            newQuery.messageType().eq("ACK^ALL^ACK");
            return newQuery.getUniqueResult();
        } else {
            return validationParameters;
        }
    }

    private static ValidationParametersQuery buildQueryForResponse(TransactionInstance transactionInstance, MessageInstance response) {
        ValidationParametersQuery query = new ValidationParametersQuery();
        query.actor().keyword().eq(response.getIssuingActor().getKeyword());
        query.transaction().keyword().eq(transactionInstance.getTransaction().getKeyword());
        query.domain().keyword().eq(transactionInstance.getDomain().getKeyword());
        return query;
    }

    /**
     * <p>getSuitableParser.</p>
     *
     * @param messageEncoding a {@link net.ihe.gazelle.hl7.validator.client.MessageEncoding} object.
     *
     * @return a {@link ca.uhn.hl7v2.parser.Parser} object.
     */
    public static Parser getSuitableParser(MessageEncoding messageEncoding) {
        switch (messageEncoding) {
            case XML:
                return DefaultXMLParser.getInstanceWithNoValidation();
            default:
                return PipeParser.getInstanceWithNoValidation();
        }
    }

    public static void ExtractIds(Message request, Message response, TransactionInstance transactionInstance) {
        extractMessageControlId(request, transactionInstance.getRequest());
        extractMessageControlId(response, transactionInstance.getResponse());
    }

    public static void extractMessageControlId(Message hl7v2Message, MessageInstance messageInstance) {
        if (hl7v2Message != null) {
            Terser requestParser = new Terser(hl7v2Message);
            try {
                String requestMessageControlId = requestParser.get(MSH_10_PATH);
                MessageInstanceMetadata messageInstanceMetadataReq = new MessageInstanceMetadata(messageInstance,
                        MessageInstanceMetadata.MESSAGE_ID_LABEL, requestMessageControlId);
                messageInstanceMetadataReq.save();
            }catch (HL7Exception e){
                LOG.debug("Cannot extract messageControlId from message: " + e.getMessage());
            }
        }
    }


    public static void setMessageControlId(String messageControlId, MessageInstance request) {
        MessageInstanceMetadata messageInstanceMetadataReq = new MessageInstanceMetadata(request,
                MessageInstanceMetadata.MESSAGE_ID_LABEL, messageControlId);
        messageInstanceMetadataReq.save();
    }
}
